// Variables globales


const formularioUI = document.querySelector('#formulario');
const listaOcomprasUI = document.getElementById('listaOcompras');
let arrayOcompras = [];


    let cod = document.getElementById("cod");
    let desc = document.getElementById("desc");
    let ctdad = document.getElementById("ctdad");
    let prov = document.getElementById("prov");    



let item = {

    cod:'',    
    desc:'',   
    ctdad:'',     
    prov:'',    
    estado: false
}

// Funciones

const CrearItem = ( codUI, descUI, ctdadUI, provUI ) => {

    item = {

        codUI: codUI,
        descUI: descUI,
        ctdadUI: ctdadUI,
        provUI: provUI,

        estado: false
    }

    arrayOcompras.push(item);

    return item;
}

const SaveDB = () => {

    localStorage.setItem('ocompras', JSON.stringify(arrayOcompras));

    PintarDB();

}

const PintarDB = () => {

    listaOcomprasUI.innerHTML = '';

    arrayOcompras = JSON.parse(localStorage.getItem('ocompras'));



    if(arrayOcompras === null){
        arrayOcompras = [];
    }else{

    for(var i=0;i<arrayOcompras.length;i++){

        var codUI = arrayOcompras[i].codUI;
        var descUI = arrayOcompras[i].descUI;
        var ctdadUI = arrayOcompras[i].ctdadUI;
        var provUI = arrayOcompras[i].provUI;

        if(arrayOcompras[i].estado){

            listaOcomprasUI.innerHTML +=         
            '<table id="listaOcompras" class="table">'+
              '<tbody id="tableBody">'+
                '<tr class="success">'+
                  '<th scope="row">'+codUI+'</th>'+
                  '<td><b>'+descUI+'</b></td>'+
                  '<td>'+ctdadUI+'</td>'+
                  '<td>'+provUI+'</td>'+
                  '<td><i class="material-icons md-36 d">done</i>  <i class="material-icons md-36">delete</i></td>'+
                '</tr>'+                
              '</tbody>'+
            '</table>';            

        }else{

            listaOcomprasUI.innerHTML +=         
            '<table id="listaOcompras" class="table">'+
              '<tbody id="tableBody">'+
                '<tr class="warning">'+
                  '<th scope="row">'+codUI+'</th>'+
                  '<td><b>'+descUI+'</b></td>'+
                  '<td>'+ctdadUI+'</td>'+
                  '<td>'+provUI+'</td>'+
                  '<td><i class="material-icons md-36 d">done</i>  <i class="material-icons md-36">delete</i></td>'+
                '</tr>'+                
              '</tbody>'+
            '</table>';            

        }     

        }
    }

}    

const DeleteDB = (codUI) => {
    let indexArray;


    arrayOcompras.forEach((elemento, index) => {
        //console.log(index);

        if(elemento.codUI === codUI){
            indexArray = index;
        }

    });

    arrayOcompras.splice(indexArray,1);
    SaveDB();

}

const EditDB = (codUI) => {

    let indexArray = arrayOcompras.findIndex((elemento)=>elemento.codUI === codUI)

    //console.log(arrayOcompras[indexArray])

    arrayOcompras[indexArray].estado = true;

    SaveDB();
}
    
//console.log(codUI);
//console.log(arrayOcompras);


// EventListener

formularioUI.addEventListener('submit', (e) => {
    e.preventDefault();

    let codUI = document.querySelector('#cod').value;
    let descUI = document.querySelector('#desc').value;
    let ctdadUI = document.querySelector('#ctdad').value;
    let provUI = document.querySelector('#prov').value;


    CrearItem(codUI,descUI,ctdadUI,provUI);
    SaveDB();

    //console.log(codUI,descUI,ctdadUI,provUI);

    formularioUI.reset();    
      
});

document.addEventListener('DOMContentLoaded', PintarDB);




listaOcomprasUI.addEventListener('click', (e) =>  {

    e.preventDefault();

    //console.log(e);


    if (e.target.innerHTML === 'done' || e.target.innerHTML === 'delete') {
        let texto = (e.path[2].childNodes[0].innerHTML);

        if(e.target.innerHTML === 'delete'){

            //Acción de eliminar

            DeleteDB(texto);

            //console.log(e.path[2].childNodes[0].innerHTML);
        }
        if(e.target.innerHTML === 'done'){

            //Acción de editar

            EditDB(texto);

            //console.log(e.path[2].childNodes[0].innerHTML);
        }

    }


});



